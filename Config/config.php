<?php

$config["DB"]["default"]["hostname"] = "127.0.0.1";
$config["DB"]["default"]["username"] = "test";
$config["DB"]["default"]["password"] = "test";
$config["DB"]["default"]["database"] = "xxx";
$config["DB"]["default"]["hostport"] = "3306";

$config["DB"]["master"]["hostname"] = "127.0.0.1";
$config["DB"]["master"]["username"] = "test";
$config["DB"]["master"]["password"] = "test";
$config["DB"]["master"]["database"] = "xxx";
$config["DB"]["master"]["hostport"] = "3306";

$config["Log"]            = "/tmp/logs/";
$config["MAX_SLEEP_TIME"] = 0;

return $config;
