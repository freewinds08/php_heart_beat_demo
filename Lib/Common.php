<?php

/**
 * Created by yangy.
 * Date: 2021/4/15
 * Time: 18:18
 */

/**
 * 记录和统计时间（微秒）和内存使用情况
 * 使用方法:
 * <code>
 * G('begin'); // 记录开始标记位
 * // ... 区间运行代码
 * G('end'); // 记录结束标签位
 * echo G('begin','end',6); // 统计区间运行时间 精确到小数后6位
 * echo G('begin','end','m'); // 统计区间内存使用情况
 * 如果end标记位没有定义，则会自动以当前作为标记位
 * 其中统计内存使用需要 MEMORY_LIMIT_ON 常量为true才有效
 * </code>
 *
 * @param  string  $start  开始标签
 * @param  string  $end  结束标签
 * @param  integer|string  $dec  小数位或者m
 *
 * @return mixed
 */
function G($start, $end = '', $dec = 4)
{
    static $_info = array();
    if (is_float($end)) { // 记录时间
        $_info[$start] = $end;
    } elseif (!empty($end)) { // 统计时间和内存使用
        $_info[$end] = microtime(true);
        return number_format(($_info[$end] - $_info[$start]), $dec);
    } else { // 记录时间和内存使用
        $_info[$start] = microtime(true);
    }
}

/**
 * 发送CURL
 */
function sendCurl($url, array $params = array(), $mode = 'post')
{
    $curlHandle = curl_init();
    curl_setopt($curlHandle, CURLOPT_TIMEOUT, 20);
    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
    if ($mode == 'post') {
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($curlHandle, CURLOPT_POST, true);
//        curl_setopt($curlHandle, CURLOPT_TIMEOUT, '20');
        curl_setopt($curlHandle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); //强制使用哪个版本  
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($params));
    } else {
        $url .= (strpos($url, '?') === false ? '?' : '&').http_build_query($params);
    }

    curl_setopt($curlHandle, CURLOPT_URL, $url);
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, false);
    }

    $result     = curl_exec($curlHandle);
    $curl_errno = curl_errno($curlHandle);
    $curl_error = curl_error($curlHandle);
    if ($curl_errno > 0) {
        $error = date('Y-m-d H:i:s').",".implode(",", $params).",".$curl_error;
        Log::error($error."\n");
    }
    curl_close($curlHandle);
    return $result;
}

function dd($value){
    var_dump($value);
    exit();
}