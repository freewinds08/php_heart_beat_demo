<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/8/4
 * Time: 14:36
 */
class Log
{

    /**
     * 打日志
     * @param string $msg 日志内容
     * @param string $level 日志等级
     * @param bool $wf 是否为错误日志
     */
    public static function write($msg, $level = 'LOG', $path = '.')
    {
        $logPath = Config::getConfig("Log") . $path ."/". $level . "_" . date('Ymd') . '.log';
        file_put_contents($logPath, $msg, FILE_APPEND);
    }

    /**
     * 打印warning日志
     * @param string $msg 日志信息
     */
    public static function warn($msg)
    {
        self::write($msg, 'WARN');
    }

    /**
     * 打印fatal日志
     * @param string $msg 日志信息
     */
    public static function fatal($msg)
    {
        self::write($msg, 'FATAL');
    }

    /**
     * 打印error日志
     * @param string $msg 日志信息
     */
    public static function error($msg)
    {
        self::write($msg, 'ERROR');
    }

    /**
     * 打印debug日志
     * @param string $msg 日志信息
     */
    public static function debug($msg)
    {
        self::write($msg, 'DEBUG');
    }

}