<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/8/4
 * Time: 15:00
 */
ini_set("display_errors", "On");
error_reporting(E_ALL | E_STRICT);

require_once(dirname(__FILE__)."/"."Config.php");
require_once(dirname(__FILE__)."/"."DbMysqli.php");
require_once(dirname(__FILE__)."/"."Log.php");
require_once(dirname(__FILE__)."/"."Common.php");


register_shutdown_function("fatalError");
set_error_handler("appError");
set_exception_handler("appException");

function fatalError()
{
    if ($e = error_get_last()) {
        Log::fatal('[ERROR]'.$e['message'].' in '.$e['file'].' on line '.$e['line']."\n");
    }
}

function appError($errno, $errstr, $errfile, $errline)
{
    switch ($errno) {
        case E_ERROR:
        case E_PARSE:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
            $errorStr = "$errstr ".$errfile.' on line '.$errline."\n";

            break;
        case E_STRICT:
        case E_USER_WARNING:
        case E_USER_NOTICE:
        default:
            $errorStr = "[$errno] $errstr ".$errfile.' on line '.$errline."\n";
            break;
    }

    Log::error($errorStr);
}

function appException($e)
{
    $error            = array();
    $error['message'] = $e->getMessage();
    $trace            = $e->getTrace();
    if ('throw_exception' == $trace[0]['function']) {
        $error['file'] = $trace[0]['file'];
        $error['line'] = $trace[0]['line'];
    } else {
        $error['file'] = $e->getFile();
        $error['line'] = $e->getLine();
    }
    Log::error($error['message']."\n");
}