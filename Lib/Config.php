<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/8/4
 * Time: 14:50
 */
class Config {

    static $config;
    static $lock = false;

    public function __get($name)
    {
        return self::$config[$name];
    }

    public function __set($key, $value)
    {
        self::$config[$key] = $value;
    }

    static function getConfig($name)
    {
        if (!self::$lock) {
            self::$lock   = true;
            self::$config = require_once(dirname(dirname(__FILE__))."/Config/config.php");
        }

        return self::$config[$name];
    }

    public function setConfig($key, $value)
    {
        self::$config[$key] = $value;
    }

}
